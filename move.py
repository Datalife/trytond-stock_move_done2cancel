# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    cancelation_allowed = fields.Function(
        fields.Boolean('Cancelation allowed'),
        'on_change_with_cancelation_allowed')

    @classmethod
    def __setup__(cls):
        super(Move, cls).__setup__()
        cls._deny_modify_done_cancel.remove('state')
        cls._deny_modify_done_cancel.remove('effective_date')
        cls._transitions |= set((
            ('done', 'cancelled'),
            ('cancelled', 'draft')))
        cls._buttons['draft']['invisible'] &= (Eval('state') != 'cancelled')
        cls._buttons['cancel']['invisible'] = ~Eval('cancelation_allowed')
        cls._buttons['cancel']['depends'].append('cancelation_allowed')

    @classmethod
    def cancel(cls, records):
        _check_origin = Transaction().context.get('check_origin', True)
        _check_shipment = Transaction().context.get('check_shipment', True)
        skip_origins = cls.skip_origins_to_check()
        for record in records:
            if record.state != 'done':
                continue
            if ((_check_origin
                    and record.origin
                    and record.origin.__name__ not in skip_origins
                    )
                    or (_check_shipment and record.shipment)):
                raise UserError(gettext(
                    'stock_move_done2cancel.msg_cancel_origin'))
        super().cancel(records)

    @fields.depends('state', 'origin', 'shipment')
    def on_change_with_cancelation_allowed(self, name=None):
        return (self.state != 'cancelled'
            and not self.shipment and not self.origin)

    @classmethod
    def skip_origins_to_check(cls):
        return ['stock.move']


class ShipmentMixin(object):
    __slots__ = ()

    @classmethod
    def __setup__(cls):
        super(ShipmentMixin, cls).__setup__()
        cls._transitions.add(('done', 'cancelled'))
        cls._buttons['cancel']['invisible'] = (Eval('state') == 'cancelled')

    @classmethod
    def cancel(cls, records):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        User = pool.get('res.user')

        done2cancel = [r for r in records
            if r.state in cls._get_states_to_cancel()]

        def in_group():
            group = ModelData.get_id(*cls._get_force_cancel_group_id())
            transaction = Transaction()
            user_id = transaction.user
            if user_id == 0:
                user_id = transaction.context.get('user', user_id)
            if user_id == 0:
                return True
            user = User(user_id)
            return group in list(map(int, user.groups))

        context = {}
        if done2cancel:
            if not in_group() and \
                    Transaction().context.get('check_shipment', True):
                raise UserError(gettext(
                    'stock_move_done2cancel.msg_cannot_force_%s_cancel' %
                    '_'.join(cls.__name__.split('.')[1:])))
            context['check_shipment'] = False

        with Transaction().set_context(**context):
            super(ShipmentMixin, cls).cancel(records)

    @classmethod
    def _get_states_to_cancel(cls):
        return ('done', )

    @classmethod
    def _get_force_cancel_group_id(cls):
        pass


class ShipmentOut(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    @classmethod
    def _get_force_cancel_group_id(cls):
        return ('stock_move_done2cancel',
            'group_stock_force_cancel_shipment_out')


class ShipmentOutReturn(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @classmethod
    def __setup__(cls):
        super(ShipmentOutReturn, cls).__setup__()
        cls._transitions.add(('done', 'cancelled'))

    @classmethod
    def _get_force_cancel_group_id(cls):
        return ('stock_move_done2cancel',
            'group_stock_force_cancel_shipment_out')


class ShipmentIn(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    @classmethod
    def _get_force_cancel_group_id(cls):
        return ('stock_move_done2cancel',
            'group_stock_force_cancel_shipment_in')

    @classmethod
    def _get_states_to_cancel(cls):
        return super(ShipmentIn, cls)._get_states_to_cancel() + ('received',)


class ShipmentInReturn(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'

    @classmethod
    def _get_force_cancel_group_id(cls):
        return ('stock_move_done2cancel',
            'group_stock_force_cancel_shipment_in')


class ShipmentInternal(ShipmentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        cls._buttons['cancel']['invisible'] = Eval('state').in_(
            ['cancelled', 'shipped'])

    @classmethod
    def _get_force_cancel_group_id(cls):
        return ('stock_move_done2cancel',
            'group_stock_force_cancel_internal_shipment')
